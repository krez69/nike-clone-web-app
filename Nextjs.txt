1/install le next.js dans un projet (utiliser bash dans le terminal et version Node js min 18.18)
 - create-next-app@14.0.4
 
 ensuite des questions pour parametrer le projet :
 
Ok to proceed? (y) y
√ What is your project named? ... nike-clone-web-app
√ Would you like to use TypeScript? ... Yes 
√ Would you like to use ESLint? ... Yes 
√ Would you like to use Tailwind CSS? ... Yes
√ Would you like to use `src/` directory? ... No (pour pas tout mettre dedans le dossier src)
√ Would you like to use App Router? (recommended) ... No 
√ Would you like to customize the default import alias (@/*)? ... No

2/Rentrer dans le projet : cd nike-clone-web-app 
  démarrer npm dans le terminal : npm run dev
  
3/Architecture des dossiers et fichier :
 3.1
	- creer des dossier src -> views et components (séparer)
	- creer un ficher HomePageView.tsx dans le dossier views
	- Dans ce fichier ecrit RFCE pour importer la function React
	
 3.2
	- Aller dans le dossier 'pages' et fichier 'index.tsx'
	- supprimer tout dans le 'return' et importer la HomePageView comme ceci
	
	export default function Home() {
  return (
    <HomePageView/>
  )
  
 3.3
	- Dans le dossier 'styles' suprimer les lignes de code du mode Dark dans le fichier 'globals.css' 
  
:root {
  --foreground-rgb: 0, 0, 0;
  --background-start-rgb: 214, 219, 220;
  --background-end-rgb: 255, 255, 255;
}

@media (prefers-color-scheme: dark) {
  :root {
    --foreground-rgb: 255, 255, 255;
    --background-start-rgb: 0, 0, 0;
    --background-end-rgb: 0, 0, 0;
  }
}

body {
  color: rgb(var(--foreground-rgb));
  background: linear-gradient(
      to bottom,
      transparent,
      rgb(var(--background-end-rgb))
    )
    rgb(var(--background-start-rgb));
}

 3.4
	- dans le ficher config tailwind.config.ts modifier la ligne 6 en mettant ceci
		 './src/**/*.{js,ts,jsx,tsx,mdx}',


	
	
	