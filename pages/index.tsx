import Image from 'next/image'
import { Inter } from 'next/font/google'
import HomePageView from '@/src/views/HomePageView'
import AboutNav from '@/src/components/Layouts/AboutNav'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <HomePageView />
  )
}
